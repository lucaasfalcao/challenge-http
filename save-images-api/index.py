import requests

def awesome_api(key):
    response = requests.get('https://list.ly/api/v4/search/image?q='+key)
    data = response.json()
    data_results = data['results']
    data_image = []
    for i in data_results:
        data_image.append(i['image'])
    for j in range(len(data_image)):
        r = requests.get(data_image[j])
        with open(f"{key}_photo{j+1}.jpg", 'wb') as f:
            f.write(r.content)
    print('Download sucessful!')
    
